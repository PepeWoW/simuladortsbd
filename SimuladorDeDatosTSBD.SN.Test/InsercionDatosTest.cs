﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimuladorDeDatosTSBD.SN.COMMON.Entidades;
using SimuladorDeDatosTSBD.SN.COMMON.Interfaces;
using SimuladorDeDatosTSBD.SN.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.SN.Test
{
    [TestClass]
    public class InsercionDatosTest
    {
        IGenericRepository<Nombre> _nombresRepository;
        IGenericRepository<Apellido> _apellidosRepository;
        public InsercionDatosTest()
        {
            _nombresRepository = new GenericRepository<Nombre>();
            _apellidosRepository = new GenericRepository<Apellido>();
        }

        [TestMethod]
        public void InsercionNombres()
        {
            string pathNombresHombres = @"C:\Users\pepe_\Downloads\hombres.csv";
            string pathNombresMujeres = @"C:\Users\pepe_\Downloads\mujeres.csv";
            try
            {
                int i = 0;
                using (StreamReader streamReader = new StreamReader(pathNombresHombres))
                {
                    List<string> filas = streamReader.ReadToEnd().Split(new char[] { '\n', '\r' }).ToList();
                    foreach (string fila in filas)
                    {
                        if (i > 0)
                        {
                            string[] datos = fila.Split(',');
                            if (!string.IsNullOrEmpty(datos[0]) && !string.IsNullOrEmpty(datos[1]))
                            {
                                _nombresRepository.Create(new Nombre()
                                {
                                    NombrePersona = datos[0],
                                    Frecuencia = int.Parse(datos[1]),
                                    Sexo = "H"
                                });
                            }
                        }
                        i++;
                    }
                }
                i = 0;
                using (StreamReader streamReader = new StreamReader(pathNombresMujeres))
                {
                    List<string> filas = streamReader.ReadToEnd().Split(new char[] { '\n', '\r' }).ToList();
                    foreach (string fila in filas)
                    {
                        if (i > 0)
                        {
                            string[] datos = fila.Split(',');
                            if (!string.IsNullOrEmpty(datos[0]) && !string.IsNullOrEmpty(datos[1]))
                            {
                                _nombresRepository.Create(new Nombre()
                                {
                                    NombrePersona = datos[0],
                                    Frecuencia = int.Parse(datos[1]),
                                    Sexo = "M"
                                });
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                Assert.IsTrue(!string.IsNullOrEmpty(ex.Message), ex.Message);
            }
        }

        [TestMethod]
        public void InsercionApellidos()
        {
            string pathApellidos = @"C:\Users\pepe_\Downloads\apellidos.csv";
            try
            {
                using (StreamReader streamReader = new StreamReader(pathApellidos))
                {
                    int i = 0;
                    List<string> filas = streamReader.ReadToEnd().Split(new char[] { '\n', '\r' }).ToList();
                    foreach (string fila in filas)
                    {
                        if (i > 0)
                        {
                            string[] datos = fila.Split(',');
                            if (!string.IsNullOrEmpty(datos[0]) && !string.IsNullOrEmpty(datos[1]) && !string.IsNullOrEmpty(datos[2]))
                            {
                                _apellidosRepository.Create(new Apellido()
                                {
                                    ApellidoPersona = datos[0],
                                    FrecuenciaPrimerApellido = int.Parse(datos[1]),
                                    FrecuenciaSegundoApellido = int.Parse(datos[2])
                                });
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                Assert.IsTrue(!string.IsNullOrEmpty(ex.Message), ex.Message);
            }
        }
    }
}
