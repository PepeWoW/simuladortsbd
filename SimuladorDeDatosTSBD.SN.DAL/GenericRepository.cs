﻿using MongoDB.Bson;
using MongoDB.Driver;
using SimuladorDeDatosTSBD.SN.COMMON.Entidades;
using SimuladorDeDatosTSBD.SN.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SimuladorDeDatosTSBD.SN.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _db;

        public GenericRepository()
        {
            //_client = new MongoClient("mongodb://Jose:peperu111298@cluster0-shard-00-00-8olqi.mongodb.net:27017,cluster0-shard-00-01-8olqi.mongodb.net:27017,cluster0-shard-00-02-8olqi.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
            _client = new MongoClient(new MongoUrl("mongodb://user01:user01@ds251618.mlab.com:51618/nombresdb?retryWrites=false"));


            _db = _client.GetDatabase("nombresdb");
        }

        private IMongoCollection<T> Collection() => _db.GetCollection<T>(typeof(T).Name);
        public string Error { get; private set; }

        public T Create(T entidad)
        {
            try
            {
                entidad.Id = ObjectId.GenerateNewId().ToString();
                Collection().InsertOne(entidad);
                Error = "";
                return entidad;
            }
            catch (Exception e)
            {
                Error = e.Message;
                return null;
            }

        }

        public bool Delete(string id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(entidad => entidad.Id == id).DeletedCount;
                Error = r == 1 ? "" : "No se eliminó";
                return r == 1;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public IEnumerable<T> Read
        {
            get
            {
                try
                {
                    return Collection().AsQueryable();
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public T Update(T entidadModificar)
        {
            try
            {
                int r = (int)Collection().ReplaceOne(entidad => entidad.Id == entidadModificar.Id, entidadModificar).ModifiedCount;
                Error = r == 1 ? "" : "No se modificó";
                return r == 1 ? entidadModificar : null;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado) => Read.Where(predicado.Compile());

        public T SearchById(string id) => Read.Where(entidad => entidad.Id.ToString() == id).SingleOrDefault();
    }
}
