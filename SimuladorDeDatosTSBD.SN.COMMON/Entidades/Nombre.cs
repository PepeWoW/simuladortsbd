﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.SN.COMMON.Entidades
{
    public class Nombre:BaseDTO
    {
        public string NombrePersona { get; set; }
        public int Frecuencia { get; set; }
        public string Sexo { get; set; }
    }
}
