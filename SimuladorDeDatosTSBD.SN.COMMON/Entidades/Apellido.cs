﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.SN.COMMON.Entidades
{
    public class Apellido:BaseDTO
    {
        public string ApellidoPersona { get; set; }
        public int FrecuenciaPrimerApellido { get; set; }
        public int FrecuenciaSegundoApellido { get; set; }
    }
}
