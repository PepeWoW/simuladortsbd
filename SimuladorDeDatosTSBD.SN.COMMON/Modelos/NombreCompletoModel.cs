﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.SN.COMMON.Modelos
{
    public class NombreCompletoModel
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Sexo { get; set; }
    }
}
