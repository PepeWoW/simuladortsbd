using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimuladorDeDatosTSBD.BIZ;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using SimuladorDeDatosTSBD.Consume.API;
using SimuladorDeDatosTSBD.SN.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SimuladorDeDatosTSBD.PobladorDatos
{
    [TestClass]
    public class UnitTest1
    {
        ISemestreManager _semestreManager;
        IMateriaManager _materiaManager;
        IUnidadManager _unidadManager;
        ICriterioDeEvaluacionManager _criterioDeEvaluacionManager;
        IGeneracionManager _generacionManager;
        IGrupoManager _grupoManager;
        IAlumnoManager _alumnoManager;
        public UnitTest1()
        {
            _semestreManager = FabricManager.SemestreManager();
            _materiaManager = FabricManager.MateriaManager();
            _unidadManager = FabricManager.UnidadManager();
            _criterioDeEvaluacionManager = FabricManager.CriterioDeEvaluacionManager();
            _generacionManager = FabricManager.GeneracionManager();
            _grupoManager = FabricManager.GrupoManager();
            _alumnoManager = FabricManager.AlumnoManager();
        }

        [TestMethod]
        public void SemestresTest()
        {
            Assert.IsTrue(_semestreManager.Insertar(new Semestre() { NombreSemestre = "Primer Semestre" }) != null, _semestreManager.Error);
            Assert.IsTrue(_semestreManager.Insertar(new Semestre() { NombreSemestre = "Segundo Semestre" }) != null, _semestreManager.Error);
            Assert.IsTrue(_semestreManager.Insertar(new Semestre() { NombreSemestre = "Tercer Semestre" }) != null, _semestreManager.Error);
            Assert.IsTrue(_semestreManager.Insertar(new Semestre() { NombreSemestre = "Cuarto Semestre" }) != null, _semestreManager.Error);
            Assert.IsTrue(_semestreManager.Insertar(new Semestre() { NombreSemestre = "Quinto Semestre" }) != null, _semestreManager.Error);
            Assert.IsTrue(_semestreManager.Insertar(new Semestre() { NombreSemestre = "Sexto Semestre" }) != null, _semestreManager.Error);
            Assert.IsTrue(_semestreManager.Insertar(new Semestre() { NombreSemestre = "Septimo Semestre" }) != null, _semestreManager.Error);
            Assert.IsTrue(_semestreManager.Insertar(new Semestre() { NombreSemestre = "Octavo Semestre" }) != null, _semestreManager.Error);
            Assert.IsTrue(_semestreManager.Insertar(new Semestre() { NombreSemestre = "Noveno Semestre" }) != null, _semestreManager.Error);
        }

        [TestMethod]
        public void MateriasTest()
        {
            bool esMateria = false;
            bool esUnidad = false;
            bool esCriterio = false;
            try
            {
                using (StreamReader streamReader = new StreamReader(@"C:\Users\pepe_\OneDrive\Escritorio\simuladortsbd\MateriasYMas.csv"))
                {
                    Materia ultimaMateria = null;
                    Unidad ultimaUnidad = null;
                    List<string> filas = streamReader.ReadToEnd().Split(new char[] { '\n', '\r' }).ToList();
                    foreach (string fila in filas)
                    {
                        if (!string.IsNullOrEmpty(fila))
                        {
                            string[] datos = fila.Split(',');
                            esMateria = !string.IsNullOrEmpty(datos[0]) && !string.IsNullOrEmpty(datos[1]);
                            esUnidad = string.IsNullOrEmpty(datos[0]) && !string.IsNullOrEmpty(datos[1]);
                            esCriterio = string.IsNullOrEmpty(datos[0]) && string.IsNullOrEmpty(datos[1]);
                            if (esMateria)
                            {
                                Semestre semestre = _semestreManager.BuscarPorNombreExacto(datos[1]);
                                ultimaMateria = _materiaManager.Insertar(new Materia()
                                {
                                    NombreMateria = datos[0],
                                    IdSemestre = semestre.Id
                                });
                                Assert.IsTrue(ultimaMateria != null, _materiaManager.Error);
                            }
                            if (esUnidad)
                            {
                                ultimaUnidad = _unidadManager.Insertar(new Unidad()
                                {
                                    NumeroUnidad = int.Parse(datos[1]),
                                    IdMateria = ultimaMateria.Id
                                });
                                Assert.IsTrue(ultimaUnidad != null, _unidadManager.Error);
                            }
                            if (esCriterio)
                            {
                                Assert.IsTrue(_criterioDeEvaluacionManager.Insertar(new CriterioDeEvaluacion()
                                {
                                    Nombre = datos[2],
                                    Ponderacion = int.Parse(datos[3]),
                                    IdUnidad = ultimaUnidad.Id
                                }) != null, _criterioDeEvaluacionManager.Error);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Assert.IsTrue(string.IsNullOrEmpty(ex.Message), ex.Message);
            }
        }

        [TestMethod]
        public void GeneracionesTest()
        {
            Assert.IsTrue(_generacionManager.Insertar(new Generacion() { NumGeneracion = 1, AnioInicio = 2010, AnioFin = 2014 }) != null, _generacionManager.Error);
            Assert.IsTrue(_generacionManager.Insertar(new Generacion() { NumGeneracion = 2, AnioInicio = 2011, AnioFin = 2015 }) != null, _generacionManager.Error);
            Assert.IsTrue(_generacionManager.Insertar(new Generacion() { NumGeneracion = 3, AnioInicio = 2012, AnioFin = 2016 }) != null, _generacionManager.Error);
            Assert.IsTrue(_generacionManager.Insertar(new Generacion() { NumGeneracion = 4, AnioInicio = 2013, AnioFin = 2017 }) != null, _generacionManager.Error);
            Assert.IsTrue(_generacionManager.Insertar(new Generacion() { NumGeneracion = 5, AnioInicio = 2014, AnioFin = 2018 }) != null, _generacionManager.Error);
            Assert.IsTrue(_generacionManager.Insertar(new Generacion() { NumGeneracion = 6, AnioInicio = 2015, AnioFin = 2019 }) != null, _generacionManager.Error);
            Assert.IsTrue(_generacionManager.Insertar(new Generacion() { NumGeneracion = 7, AnioInicio = 2016, AnioFin = 2020 }) != null, _generacionManager.Error);
            Assert.IsTrue(_generacionManager.Insertar(new Generacion() { NumGeneracion = 8, AnioInicio = 2017, AnioFin = 2021 }) != null, _generacionManager.Error);
            Assert.IsTrue(_generacionManager.Insertar(new Generacion() { NumGeneracion = 9, AnioInicio = 2018, AnioFin = 2022 }) != null, _generacionManager.Error);
            Assert.IsTrue(_generacionManager.Insertar(new Generacion() { NumGeneracion = 10, AnioInicio = 2019, AnioFin = 2023 }) != null, _generacionManager.Error);
        }

        [TestMethod]
        public void GruposTest()
        {
            Random numeroGrupos = new Random();
            Assert.IsTrue(_grupoManager.Insertar(new Grupo(){Numero = numeroGrupos.Next(1, 6)}) != null, _grupoManager.Error);
            IEnumerable<Grupo> grupoGeneracion = _grupoManager.BuscarPorIdGeneracion("");
            Assert.IsTrue(grupoGeneracion != null, "No se encontraron grupos con el id de Generación proporcionado");
            

        }

        [TestMethod]
        public void AlumnosTest()
        {
            NombreCompletoModel nombre = new ConsumoClass().GetAlumnosForecast("H");
            NombreCompletoModel nombre2 = new ConsumoClass().GetAlumnosForecast("M");

            Random aleatorioMatricula = new Random();

            bool sexo = false;

            Generacion generacion = new Generacion();

            Assert.IsTrue(_alumnoManager.Insertar(new Alumno() {
                Matricula = aleatorioMatricula.Next(10000000, 19000000).ToString(),
                Egresado = false,
                Titulado = false,
                AnioTermino = generacion.AnioFin,
                Sexo = sexo.ToString(),
                Nombre = ,
                Apellidos = ,
            }) != null, _generacionManager.Error);

            IEnumerable<Alumno> alumnoGeneracion = _alumnoManager.BuscarPorIdGeneracion("");
            Assert.IsTrue(alumnoGeneracion != null, "No se encontraron alumnos con el id de Generación proporcionado");

            IEnumerable<Alumno> alumnoGrupo = _alumnoManager.BuscarPorIdGrupo("");
            Assert.IsTrue(alumnoGrupo != null, "No se encontraron alumnos con el id de Grupo proporcionado");

            


        }
    }
}

