﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class MateriaManager : GenericManager<Materia>, IMateriaManager
    {
        public MateriaManager(IGenericRepository<Materia> repository) : base(repository)
        {
        }

        public IEnumerable<Materia> BuscarPorNombre(string nombreMateria) => _repository.Query(m => m.NombreMateria == nombreMateria);
    }
}
