﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class CalificacionSegundasManager : GenericManager<CalificacionSegundas>, ICalificacionSegundasManager
    {
        public CalificacionSegundasManager(IGenericRepository<CalificacionSegundas> repository) : base(repository)
        {
        }

        public IEnumerable<CalificacionSegundas> BuscarPorCalificacionFinal(int calificacionFinal) => _repository.Query(c => c.CalificacionFinal == calificacionFinal);

        public IEnumerable<CalificacionSegundas> BuscarPorIdMateria(string idMateria) => _repository.Query(c => c.IdMateria == idMateria);


        public IEnumerable<CalificacionSegundas> BuscarPorLosQuePasan(bool pasaONoPasa) => _repository.Query(c => c.Pasa == pasaONoPasa);


        public IEnumerable<CalificacionSegundas> BuscarPorMatricula(string matricula) => _repository.Query(c => c.Matricula == matricula);

    }
}
