﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class UnidadManager : GenericManager<Unidad>, IUnidadManager
    {
        public UnidadManager(IGenericRepository<Unidad> repository) : base(repository)
        {
        }

        public IEnumerable<Unidad> BuscarPorIdMateria(string idMateria) => _repository.Query(u => u.IdMateria == idMateria);

        public IEnumerable<Unidad> BuscarPorNumeroUnidad(int numeroUnidad) => _repository.Query(u => u.NumeroUnidad == numeroUnidad);
    }
}