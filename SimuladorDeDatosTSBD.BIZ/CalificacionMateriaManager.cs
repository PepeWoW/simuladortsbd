﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class CalificacionMateriaManager : GenericManager<CalificacionMateria>, ICalificacionMateriaManager
    {
        public CalificacionMateriaManager(IGenericRepository<CalificacionMateria> repository) : base(repository)
        {
        }

        public IEnumerable<CalificacionMateria> BuscarPorCalificacionFinal(int calificacionFinal) => _repository.Query(c => c.CalificacionFinal == calificacionFinal);

        public IEnumerable<CalificacionMateria> BuscarPorMatricula(string matricula) => _repository.Query(c => c.Matricula == matricula);
    }
}
