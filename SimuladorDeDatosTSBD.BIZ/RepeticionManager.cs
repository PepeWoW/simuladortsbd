﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class RepeticionManager : GenericManager<Repeticion>, IRepeticionManager
    {
        public RepeticionManager(IGenericRepository<Repeticion> repository) : base(repository)
        {
        }

        public IEnumerable<Repeticion> BuscarPorAprobadosReprobados(bool pasaONoPasa) => _repository.Query(r => r.Pasa == pasaONoPasa);

        public IEnumerable<Repeticion> BuscarPorCalificacion(int calificacion) => _repository.Query(r => r.Calificacion == calificacion);

        public IEnumerable<Repeticion> BuscarPorIdMateria(string idMateria) => _repository.Query(r => r.IdMateria == idMateria);

        public IEnumerable<Repeticion> BuscarPorMatricula(string matricula) => _repository.Query(r => r.Matricula == matricula);
    }
}
