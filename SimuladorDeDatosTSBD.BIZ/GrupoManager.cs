﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class GrupoManager : GenericManager<Grupo>, IGrupoManager
    {
        public GrupoManager(IGenericRepository<Grupo> repository) : base(repository)
        {
        }

        public IEnumerable<Grupo> BuscarPorIdGeneracion(string idGeneracion) => _repository.Query(g => g.IdGeneracion == idGeneracion);

        public IEnumerable<Grupo> BuscarPorNumGrupo(int numeroGrupo) => _repository.Query(g => g.Numero == numeroGrupo);
    }
}
