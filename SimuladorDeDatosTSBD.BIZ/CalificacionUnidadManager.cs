﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class CalificacionUnidadManager : GenericManager<CalificacionUnidad>, ICalificacionUnidadManager
    {
        public CalificacionUnidadManager(IGenericRepository<CalificacionUnidad> repository) : base(repository)
        {
        }

        public IEnumerable<CalificacionUnidad> BuscarPorCalificacion(int calificacion) => _repository.Query(c => c.Calificacion == calificacion);

        public IEnumerable<CalificacionUnidad> BuscarPorIdUnidad(string idUnidad) => _repository.Query(c => c.IdUnidad == idUnidad);

        public IEnumerable<CalificacionUnidad> BuscarPorMatricula(string matricula) => _repository.Query(c => c.Matricula == matricula);
    }
}
