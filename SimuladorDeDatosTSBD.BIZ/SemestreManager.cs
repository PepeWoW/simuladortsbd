﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class SemestreManager : GenericManager<Semestre>, ISemestreManager
    {
        public SemestreManager(IGenericRepository<Semestre> repository) : base(repository)
        {
        }

        public Semestre BuscarPorNombreExacto(string nombre) => _repository.Query(s => s.NombreSemestre == nombre).SingleOrDefault();
    }
}
