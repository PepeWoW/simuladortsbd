﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        internal IGenericRepository<T> _repository;
        public GenericManager(IGenericRepository<T> repository)
        {
            _repository = repository;
        }

        public string Error => _repository.Error;

        public IEnumerable<T> ObtenerTodos => _repository.Read;

        public T Actualizar(T entidad) => _repository.Update(entidad);


        public T BuscarPorId(string id) => _repository.SearchById(id);

        public bool Eliminar(string id) => _repository.Delete(id);

        public T Insertar(T entidad) => _repository.Create(entidad);
    }
}
