﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class CriterioDeEvaluacionManager : GenericManager<CriterioDeEvaluacion>, ICriterioDeEvaluacionManager
    {
        public CriterioDeEvaluacionManager(IGenericRepository<CriterioDeEvaluacion> repository) : base(repository)
        {
        }

        public IEnumerable<CriterioDeEvaluacion> BuscarPorIdMateria(string idUnidad) => _repository.Query(c => c.IdUnidad == idUnidad);

        public IEnumerable<CriterioDeEvaluacion> BuscarPorNombre(string nombre) => _repository.Query(c => c.Nombre == nombre);

        public IEnumerable<CriterioDeEvaluacion> BuscarPorPonderacion(int ponderacion) => _repository.Query(c => c.Ponderacion == ponderacion);
    }
}
