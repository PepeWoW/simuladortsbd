﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.DAL.Mongo;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public static class FabricManager
    {
        public static AlumnoManager AlumnoManager() => new AlumnoManager(new GenericRepository<Alumno>());
        public static CalificacionMateriaManager CalificacionMateriaManager() => new CalificacionMateriaManager(new GenericRepository<CalificacionMateria>());
        public static CalificacionPorCriterioManager CalificacionPorCriterioManager() => new CalificacionPorCriterioManager(new GenericRepository<CalificacionPorCriterio>());
        public static CalificacionSegundasManager CalificacionSegundasManager() => new CalificacionSegundasManager(new GenericRepository<CalificacionSegundas>());
        public static CalificacionUnidadManager CalificacionUnidadManager() => new CalificacionUnidadManager(new GenericRepository<CalificacionUnidad>());
        public static CriterioDeEvaluacionManager CriterioDeEvaluacionManager() => new CriterioDeEvaluacionManager(new GenericRepository<CriterioDeEvaluacion>());
        public static GeneracionManager GeneracionManager() => new GeneracionManager(new GenericRepository<Generacion>());
        public static GrupoManager GrupoManager() => new GrupoManager(new GenericRepository<Grupo>());
        public static MateriaManager MateriaManager() => new MateriaManager(new GenericRepository<Materia>());
        public static RepeticionManager RepeticionManager() => new RepeticionManager(new GenericRepository<Repeticion>());
        public static SemestreManager SemestreManager() => new SemestreManager(new GenericRepository<Semestre>());
        public static UnidadManager UnidadManager() => new UnidadManager(new GenericRepository<Unidad>());
    }
}
