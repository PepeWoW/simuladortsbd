﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class CalificacionPorCriterioManager : GenericManager<CalificacionPorCriterio>, ICalificacionPorCriterioManager
    {
        public CalificacionPorCriterioManager(IGenericRepository<CalificacionPorCriterio> repository) : base(repository)
        {
        }

        public IEnumerable<CalificacionPorCriterio> BuscarPorCalificacion(int calificacion) => _repository.Query(c => c.Calificacion == calificacion);

        public IEnumerable<CalificacionPorCriterio> BuscarPorIdMateria(string idMateria) => _repository.Query(c => c.IdMateria == idMateria);

        public IEnumerable<CalificacionPorCriterio> BuscarPorMatricula(string matricula) => _repository.Query(c => c.Matricula == matricula);

        public IEnumerable<CalificacionPorCriterio> BuscarPorNombre(string nombre) => _repository.Query(c => c.NombreCriterio == nombre);

        public IEnumerable<CalificacionPorCriterio> BuscarPorNumeroUnidad(int numeroUnidad) => _repository.Query(c => c.NumUnidad == numeroUnidad);
    }
}
