﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class GeneracionManager : GenericManager<Generacion>, IGeneracionManager
    {
        public GeneracionManager(IGenericRepository<Generacion> repository) : base(repository)
        {
        }

        public IEnumerable<Generacion> BuscarPorGeneracion(int anioInicio, int anioFin) => _repository.Query(g=>g.AnioInicio==anioInicio&&g.AnioFin==anioFin);
    }
}
