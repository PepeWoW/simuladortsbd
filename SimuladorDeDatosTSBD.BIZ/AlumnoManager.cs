﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimuladorDeDatosTSBD.BIZ
{
    public class AlumnoManager : GenericManager<Alumno>, IAlumnoManager
    {
        public AlumnoManager(IGenericRepository<Alumno> repository) : base(repository)
        {
        }

        public IEnumerable<Alumno> BuscarPorAnioTermino(int anioTermino) => _repository.Query(a => a.AnioTermino == anioTermino);

        public IEnumerable<Alumno> BuscarPorApellido(string apellido) => _repository.Query(a => a.Apellidos.Contains(apellido));

        public IEnumerable<Alumno> BuscarPorEgresado(bool egresado) => _repository.Query(a => a.Egresado == egresado);

        public IEnumerable<Alumno> BuscarPorIdGeneracion(string idGeneracion) => _repository.Query(a => a.IdGeneracion == idGeneracion);

        public IEnumerable<Alumno> BuscarPorIdGrupo(string idGrupo) => _repository.Query(a => a.IdGrupo == idGrupo);

        public Alumno BuscarPorMatricula(string matricula) => _repository.Query(a => a.Matricula == matricula).SingleOrDefault();

        public IEnumerable<Alumno> BuscarPorNombre(string nombre) => _repository.Query(a => a.Nombre.Contains(nombre));

        public IEnumerable<Alumno> BuscarPorSexo(string sexo) => _repository.Query(a => a.Sexo == sexo);

        public IEnumerable<Alumno> BuscarPorTitulado(bool titulado) => _repository.Query(a => a.Titulado == titulado);
    }
}
