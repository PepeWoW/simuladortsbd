﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.COMMON.Interfaces;

namespace SimuladorDeDatosTSBD.DAL.Mongo
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _db;

        public GenericRepository()
        {
            //_client = new MongoClient("mongodb://Jose:peperu111298@cluster0-shard-00-00-8olqi.mongodb.net:27017,cluster0-shard-00-01-8olqi.mongodb.net:27017,cluster0-shard-00-02-8olqi.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
            _client = new MongoClient(new MongoUrl("mongodb://user1:user01@ds351628.mlab.com:51628/simuladordedatostsbd?retryWrites=false"));

        
            _db = _client.GetDatabase("simuladordedatostsbd");
        }

        private IMongoCollection<T> Collection() => _db.GetCollection<T>(typeof(T).Name);
        public string Error { get; private set; }

        public T Create(T entidad)
        {
            try
            {
                entidad.Id = ObjectId.GenerateNewId().ToString();
                entidad.FechaCreacion = DateTime.Now;
                Collection().InsertOne(entidad);
                Error = "";
                return entidad;
            }
            catch (Exception e)
            {
                Error = e.Message;
                return null;
            }

        }

        public bool Delete(string id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(entidad => entidad.Id == id).DeletedCount;
                Error = r == 1 ? "" : "No se eliminó";
                return r == 1;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public IEnumerable<T> Read
        {
            get
            {
                try
                {
                    return Collection().AsQueryable();
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public T Update(T entidadModificar)
        {
            try
            {
                int r = (int)Collection().ReplaceOne(entidad => entidad.Id == entidadModificar.Id, entidadModificar).ModifiedCount;
                Error = r == 1 ? "" : "No se modificó";
                return r == 1 ? entidadModificar : null;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado) => Read.Where(predicado.Compile());

        public T SearchById(string id) => Read.Where(entidad => entidad.Id.ToString() == id).SingleOrDefault();
    }
}
