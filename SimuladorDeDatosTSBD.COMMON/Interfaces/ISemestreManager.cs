﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface ISemestreManager:IGenericManager<Semestre>
    {
        Semestre BuscarPorNombreExacto(string nombre);
    }
}
