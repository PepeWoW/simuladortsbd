﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface ICalificacionMateriaManager: IGenericManager<CalificacionMateria>
    {
        IEnumerable<CalificacionMateria> BuscarPorMatricula(string matricula);

        IEnumerable<CalificacionMateria> BuscarPorCalificacionFinal(int calificacionFinal);
    }
}
