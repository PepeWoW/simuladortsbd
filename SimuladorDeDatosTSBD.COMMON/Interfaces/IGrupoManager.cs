﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface IGrupoManager: IGenericManager<Grupo>
    {
        IEnumerable<Grupo> BuscarPorNumGrupo(int numeroGrupo);
        IEnumerable<Grupo> BuscarPorIdGeneracion (string idGeneracion);
    }
}
