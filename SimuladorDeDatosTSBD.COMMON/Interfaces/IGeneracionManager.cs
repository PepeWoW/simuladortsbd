﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface IGeneracionManager:IGenericManager<Generacion>
    {
        IEnumerable<Generacion> BuscarPorGeneracion(int anioInicio, int anioFin);
    }
}
