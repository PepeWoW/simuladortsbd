﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface IAlumnoManager:IGenericManager<Alumno>
    {
        Alumno BuscarPorMatricula(string matricula);
        IEnumerable<Alumno> BuscarPorNombre(string nombre);
        IEnumerable<Alumno> BuscarPorApellido(string apellido);
        IEnumerable<Alumno> BuscarPorSexo(string sexo);
        IEnumerable<Alumno> BuscarPorAnioTermino(int anioTermino);
        IEnumerable<Alumno> BuscarPorEgresado(bool egresado);
        IEnumerable<Alumno> BuscarPorTitulado(bool titulado);
        IEnumerable<Alumno> BuscarPorIdGeneracion (string idGeneracion);
        IEnumerable<Alumno> BuscarPorIdGrupo (string idGrupo);
    }
}
