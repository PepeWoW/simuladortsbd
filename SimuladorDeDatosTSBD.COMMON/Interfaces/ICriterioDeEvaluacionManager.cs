﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface ICriterioDeEvaluacionManager:IGenericManager<CriterioDeEvaluacion>
    {
        IEnumerable<CriterioDeEvaluacion> BuscarPorNombre(string nombre);
        IEnumerable<CriterioDeEvaluacion> BuscarPorPonderacion(int ponderacion);
        IEnumerable<CriterioDeEvaluacion> BuscarPorIdMateria(string idUnidad);
    }
}
