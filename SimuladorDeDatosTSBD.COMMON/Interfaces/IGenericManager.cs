﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface IGenericManager<T> where T : BaseDTO
    {
        string Error { get; }
        T Insertar(T entidad);
        IEnumerable<T> ObtenerTodos { get; }
        T Actualizar(T entidad);
        bool Eliminar(string id);
        T BuscarPorId(string id);
    }
}
