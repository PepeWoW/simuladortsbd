﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface IUnidadManager:IGenericManager<Unidad>
    {
        IEnumerable<Unidad> BuscarPorNumeroUnidad(int numeroUnidad);
        IEnumerable<Unidad> BuscarPorIdMateria(string idMateria);
    }
}
