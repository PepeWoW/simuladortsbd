﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface ICalificacionSegundasManager: IGenericManager<CalificacionSegundas>
    {
        IEnumerable<CalificacionSegundas> BuscarPorMatricula(string matricula);
        IEnumerable<CalificacionSegundas> BuscarPorIdMateria(string idMateria);
        IEnumerable<CalificacionSegundas> BuscarPorCalificacionFinal(int calificacionFinal);
        IEnumerable<CalificacionSegundas> BuscarPorLosQuePasan(bool pasaONoPasa);
    }
}
