﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T : BaseDTO
    {
        string Error { get; }
        IEnumerable<T> Read { get; }
        T Create(T entidad);
        bool Delete(string id);
        IEnumerable<T> Query(Expression<Func<T, bool>> predicado);
        T SearchById(string id);
        T Update(T entidad);
    }
}
