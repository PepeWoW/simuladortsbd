﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface ICalificacionPorCriterioManager : IGenericManager<CalificacionPorCriterio>
    {
        IEnumerable<CalificacionPorCriterio> BuscarPorMatricula(string matricula);
        IEnumerable<CalificacionPorCriterio> BuscarPorNumeroUnidad(int numeroUnidad);
        IEnumerable<CalificacionPorCriterio> BuscarPorIdMateria(string idMateria);
        IEnumerable<CalificacionPorCriterio> BuscarPorNombre(string nombre);
        IEnumerable<CalificacionPorCriterio> BuscarPorCalificacion(int calificacion);
    }
}
