﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface IRepeticionManager:IGenericManager<Repeticion>
    {
        IEnumerable<Repeticion> BuscarPorMatricula(string matricula);
        IEnumerable<Repeticion> BuscarPorIdMateria(string idMateria);
        IEnumerable<Repeticion> BuscarPorCalificacion(int calificacion);
        IEnumerable<Repeticion> BuscarPorAprobadosReprobados(bool pasaONoPasa);
    }
}
