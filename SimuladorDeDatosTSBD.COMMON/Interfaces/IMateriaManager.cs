﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface IMateriaManager: IGenericManager<Materia>
    {
        IEnumerable<Materia> BuscarPorNombre(string nombreMateria);
    }
}
