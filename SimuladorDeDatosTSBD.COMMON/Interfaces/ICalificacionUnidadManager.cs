﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Interfaces
{
    public interface ICalificacionUnidadManager: IGenericManager<CalificacionUnidad>
    {
        IEnumerable<CalificacionUnidad> BuscarPorMatricula(string matricula);
        IEnumerable<CalificacionUnidad> BuscarPorCalificacion(int calificacion);
        IEnumerable<CalificacionUnidad> BuscarPorIdUnidad(string idUnidad);
    }
}
