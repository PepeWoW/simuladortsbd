﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class SemestreValidator:GenericValidator<Semestre>
    {
        public SemestreValidator()
        {
            RuleFor(s => s.NombreSemestre).NotNull().NotEmpty();
        }
    }
}
