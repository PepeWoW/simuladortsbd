﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class CriterioDeEvaluacionValidator : GenericValidator<CriterioDeEvaluacion>
    {
        public CriterioDeEvaluacionValidator()
        {
            RuleFor(criterioEvaluacion => criterioEvaluacion.Nombre).NotEmpty().NotNull();
            RuleFor(criterioEvaluacion => criterioEvaluacion.Ponderacion).NotEmpty().NotNull();
            RuleFor(criterioEvaluacion => criterioEvaluacion.IdUnidad).NotEmpty().NotNull();
        }
    }
}
