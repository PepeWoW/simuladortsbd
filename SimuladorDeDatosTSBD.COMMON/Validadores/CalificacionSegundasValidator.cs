﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class CalificacionSegundasValidator : GenericValidator<CalificacionSegundas>
    {
        public CalificacionSegundasValidator()
        {
            RuleFor(calificacionSegundas => calificacionSegundas.Matricula).NotEmpty().NotNull();
            RuleFor(calificacionSegundas => calificacionSegundas.IdMateria).NotEmpty().NotNull();
            RuleFor(calificacionSegundas => calificacionSegundas.CalificacionFinal).NotEmpty().NotNull();
            RuleFor(calificacionSegundas => calificacionSegundas.Pasa).NotEmpty().NotNull();
        }
    }
}
