﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class CalificacionUnidadValidator : GenericValidator<CalificacionUnidad>
    {
        public CalificacionUnidadValidator()
        {
            RuleFor(calificacionUnidad => calificacionUnidad.Matricula).NotEmpty().NotNull();
            RuleFor(calificacionUnidad => calificacionUnidad.IdUnidad).NotEmpty().NotNull();
            RuleFor(calificacionUnidad => calificacionUnidad.Calificacion).NotEmpty().NotNull();
        }
    }
}
