﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class RepeticionValidator:GenericValidator<Repeticion>
    {
        public RepeticionValidator()
        {
            RuleFor(repeticion => repeticion.Matricula).NotEmpty().NotNull();
            RuleFor(repeticion => repeticion.IdMateria).NotEmpty().NotNull();
            RuleFor(repeticion => repeticion.Calificacion).NotEmpty().NotNull();
            RuleFor(repeticion => repeticion.Pasa).NotEmpty().NotNull();
        }
    }
}
