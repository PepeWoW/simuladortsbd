﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class AlumnoValidator: GenericValidator<Alumno>
    {
        public AlumnoValidator()
        {
        RuleFor(alumno => alumno.Matricula).NotEmpty().NotNull().MaximumLength(80);
        RuleFor(alumno => alumno.Nombre).NotEmpty().NotNull().MaximumLength(80);
        RuleFor(alumno => alumno.Apellidos).NotEmpty().NotNull().MaximumLength(80);
        RuleFor(alumno => alumno.Sexo).NotEmpty().NotNull().MaximumLength(12);
        RuleFor(alumno => alumno.IdGeneracion).NotEmpty().NotNull();
        RuleFor(alumno => alumno.IdGrupo).NotEmpty().NotNull();
        RuleFor(alumno => alumno.AnioTermino).NotNull();
        RuleFor(alumno => alumno.Egresado).NotNull();
        RuleFor(alumno => alumno.Titulado).NotNull();
        }
    }
}
