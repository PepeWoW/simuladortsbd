﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class CalificacionMateriaValidator : GenericValidator<CalificacionMateria>
    {
        public CalificacionMateriaValidator()
        {
            RuleFor(calificacionMateria => calificacionMateria.Matricula).NotEmpty().NotNull();
            RuleFor(calificacionMateria => calificacionMateria.CalificacionFinal).NotEmpty().NotNull();
        }
    }
}
