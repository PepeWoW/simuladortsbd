﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class CalificacionPorCriterioValidator : GenericValidator<CalificacionPorCriterio>
    {
        public CalificacionPorCriterioValidator()
        {
            RuleFor(calificacionPorCriterio => calificacionPorCriterio.Matricula).NotEmpty().NotNull();
            RuleFor(calificacionPorCriterio => calificacionPorCriterio.IdMateria).NotEmpty().NotNull();
            RuleFor(calificacionPorCriterio => calificacionPorCriterio.NumUnidad).NotEmpty().NotNull();
            RuleFor(calificacionPorCriterio => calificacionPorCriterio.NombreCriterio).NotEmpty().NotNull();
            RuleFor(calificacionPorCriterio => calificacionPorCriterio.Calificacion).NotEmpty().NotNull();
        }
    }
}
