﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class UnidadValidator: GenericValidator<Unidad>
    {
        UnidadValidator()
        {
            RuleFor(unidad => unidad.NumeroUnidad).NotEmpty().NotNull();
            RuleFor(unidad => unidad.IdMateria).NotEmpty().NotNull();
        }
    }
}
