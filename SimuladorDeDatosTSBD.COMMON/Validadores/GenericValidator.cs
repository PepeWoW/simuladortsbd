﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class GenericValidator <T>: AbstractValidator <T> where T: BaseDTO
    {
        public GenericValidator()
        {
            RuleFor(generico => generico.Id).NotEmpty().NotNull();
            RuleFor(generico => generico.FechaCreacion).NotEmpty().NotNull();
        }
    }
}
