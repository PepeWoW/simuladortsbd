﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class GrupoValidator : GenericValidator<Grupo>
    {
        public GrupoValidator()
        {
            RuleFor(grupo => grupo.Numero).NotEmpty().NotNull();
            RuleFor(grupo => grupo.IdGeneracion).NotEmpty().NotNull();
        }
    }
}
