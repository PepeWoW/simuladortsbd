﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class MateriaValidator:GenericValidator<Materia>
    {
        MateriaValidator()
        {
            RuleFor(materia => materia.NombreMateria).NotEmpty().NotNull();
        }
    }
}
