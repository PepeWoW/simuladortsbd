﻿using FluentValidation;
using SimuladorDeDatosTSBD.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Validadores
{
    public class GeneracionValidator : GenericValidator<Generacion>
    {
        public GeneracionValidator()
        {
            RuleFor(generacion => generacion.NumGeneracion).NotEmpty().NotNull();
            RuleFor(generacion => generacion.AnioInicio).NotEmpty().NotNull();
            RuleFor(generacion => generacion.AnioFin).NotEmpty().NotNull();

        }
    }
}
