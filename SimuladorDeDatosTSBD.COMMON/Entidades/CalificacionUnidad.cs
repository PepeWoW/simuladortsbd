﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Entidades
{
    public class CalificacionUnidad:BaseDTO
    {
        public string Matricula { get; set; }
        public string IdUnidad { get; set; }
        public int Calificacion { get; set; }
    }
}
