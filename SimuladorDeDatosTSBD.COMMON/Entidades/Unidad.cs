﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Entidades
{
    public class Unidad: BaseDTO
    {
        public int NumeroUnidad { get; set; }
        public string IdMateria { get; set; }
    }
}
