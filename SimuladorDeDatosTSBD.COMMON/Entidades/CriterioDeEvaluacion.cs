﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Entidades
{
    public class CriterioDeEvaluacion:BaseDTO
    {
        public string Nombre { get; set; }
        public int Ponderacion { get; set; }
        public string IdUnidad { get; set; }
    }
}
