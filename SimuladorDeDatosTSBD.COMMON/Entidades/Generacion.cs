﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Entidades
{
    public class Generacion: BaseDTO
    {
        public int NumGeneracion { get; set; }
        public int AnioInicio { get; set; }
        public int AnioFin { get; set; }
    }
}
