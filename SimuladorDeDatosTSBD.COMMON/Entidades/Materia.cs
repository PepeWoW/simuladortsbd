﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Entidades
{
    public class Materia: BaseDTO
    {
        public string NombreMateria { get; set; }
        public string IdSemestre { get; set; }
    }
}
