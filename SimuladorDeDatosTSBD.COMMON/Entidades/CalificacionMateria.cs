﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Entidades
{
    public class CalificacionMateria:BaseDTO
    {
        public string Matricula { get; set; }
        public int CalificacionFinal { get; set; }
    }
}
