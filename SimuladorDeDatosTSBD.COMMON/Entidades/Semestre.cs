﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorDeDatosTSBD.COMMON.Entidades
{
    public class Semestre:BaseDTO
    {
        public string NombreSemestre { get; set; }
    }
}
