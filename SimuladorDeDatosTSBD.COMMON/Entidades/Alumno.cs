﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Entidades
{
    public class Alumno: BaseDTO
    {
        public string Matricula { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Sexo { get; set; }
        public string IdGeneracion { get; set; }
        public string IdGrupo { get; set; }
        public int AnioTermino { get; set; }
        public bool Egresado { get; set; }
        public bool Titulado { get; set; }
    }
}