﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Entidades
{
    public class CalificacionPorCriterio:BaseDTO    
    {
        public string Matricula { get; set; }
        public string IdMateria { get; set; }
        public int NumUnidad { get; set; }
        public string NombreCriterio { get; set; }
        public int Calificacion { get; set; }
    }
}