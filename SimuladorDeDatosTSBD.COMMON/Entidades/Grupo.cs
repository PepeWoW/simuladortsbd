﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorDeDatosTSBD.COMMON.Entidades
{
    public class Grupo:BaseDTO    
    {
        public int Numero { get; set; }
        public string IdGeneracion { get; set; }
    }
}
