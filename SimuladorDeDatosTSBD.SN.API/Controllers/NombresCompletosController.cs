﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimuladorDeDatosTSBD.SN.COMMON.Entidades;
using SimuladorDeDatosTSBD.SN.COMMON.Interfaces;
using SimuladorDeDatosTSBD.SN.COMMON.Modelos;
using SimuladorDeDatosTSBD.SN.DAL;

namespace SimuladorDeDatosTSBD.SN.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NombresCompletosController : ControllerBase
    {
        //https://simuladordedatostsbdsnapi20191204011322.azurewebsites.net/api/nombrescompletos/H
        IGenericRepository<Nombre> _nombresRepository;
        IGenericRepository<Apellido> _apellidosRepository;
        public NombresCompletosController()
        {
            _nombresRepository = new GenericRepository<Nombre>();
            _apellidosRepository = new GenericRepository<Apellido>();
        }

        [HttpGet("{id}")]
        public ActionResult<NombreCompletoModel> Get(string id)
        {
            try
            {
                Nombre nombre = ObtenerNombre(id);
                Apellido primerApellido = ObtenerApellido(1);
                Apellido segundoApellido = ObtenerApellido(2);
                return Ok(new NombreCompletoModel()
                {
                    Nombre = nombre.NombrePersona,
                    Apellidos = $"{primerApellido.ApellidoPersona} {segundoApellido.ApellidoPersona}",
                    Sexo = nombre.Sexo
                });
            }
            catch (Exception ex)
            {
                return BadRequest("Error al obtener datos");
            }
        }

        private Apellido ObtenerApellido(int v)
        {
            Apellido apellido = new Apellido();
            List<Apellido> apellidos = new List<Apellido>();
            apellidos = _apellidosRepository.Read.ToList();
            int minimo, maximo, aleatorioPosicionApellidoFrecuencia;
            List<Apellido> apellidosFrecuencia = new List<Apellido>();
            switch (v)
            {
                case 1:
                    minimo = apellidos.OrderBy(n => n.FrecuenciaPrimerApellido).FirstOrDefault().FrecuenciaPrimerApellido;
                    maximo = apellidos.OrderBy(n => n.FrecuenciaPrimerApellido).LastOrDefault().FrecuenciaPrimerApellido;
                    while (apellidosFrecuencia.Count == 0)
                    {
                        aleatorioPosicionApellidoFrecuencia = ObtenerAleatorio(minimo, maximo);
                        apellidosFrecuencia = apellidos.Where(n => n.FrecuenciaPrimerApellido == aleatorioPosicionApellidoFrecuencia).ToList();
                    }
                    if (apellidosFrecuencia.Count > 1)
                    {
                        apellido = apellidosFrecuencia[ObtenerAleatorio(0, apellidosFrecuencia.Count - 1)];
                    }
                    else
                    {
                        apellido = apellidosFrecuencia[0];
                    }
                    break;
                case 2:
                    minimo = apellidos.OrderBy(n => n.FrecuenciaSegundoApellido).FirstOrDefault().FrecuenciaSegundoApellido;
                    maximo = apellidos.OrderBy(n => n.FrecuenciaSegundoApellido).LastOrDefault().FrecuenciaSegundoApellido;
                    while (apellidosFrecuencia.Count == 0)
                    {
                        aleatorioPosicionApellidoFrecuencia = ObtenerAleatorio(minimo, maximo);
                        apellidosFrecuencia = apellidos.Where(n => n.FrecuenciaSegundoApellido == aleatorioPosicionApellidoFrecuencia).ToList();
                    }
                    if (apellidosFrecuencia.Count > 1)
                    {
                        apellido = apellidosFrecuencia[ObtenerAleatorio(0, apellidosFrecuencia.Count - 1)];
                    }
                    else
                    {
                        apellido = apellidosFrecuencia[0];
                    }
                    break;
            }
            return apellido;
        }

        private Nombre ObtenerNombre(string id)
        {
            Nombre nombre = new Nombre();
            List<Nombre> nombres = new List<Nombre>();
            nombres = _nombresRepository.Query(n => n.Sexo == id).ToList();
            if (nombres.Count > 1)
            {
                int minimo = nombres.OrderBy(n => n.Frecuencia).FirstOrDefault().Frecuencia;
                int maximo = nombres.OrderBy(n => n.Frecuencia).LastOrDefault().Frecuencia;
                List<Nombre> nombresFrecuencia = new List<Nombre>();
                while (nombresFrecuencia.Count == 0)
                {
                    int aleatorioPosicionNombreFrecuencia = ObtenerAleatorio(minimo, maximo);
                    nombresFrecuencia = nombres.Where(n => n.Frecuencia == aleatorioPosicionNombreFrecuencia).ToList();
                }
                if (nombresFrecuencia.Count > 1)
                {
                    nombre = nombresFrecuencia[ObtenerAleatorio(0, nombresFrecuencia.Count - 1)];
                }
                else
                {
                    nombre = nombresFrecuencia[0];
                }
            }
            else
            {
                nombre = nombres[0];
            }
            return nombre;
        }

        private int ObtenerAleatorio(int minimo, int maximo)
        {
            var guid = Guid.NewGuid();
            var soloNumeros = new string(guid.ToString().Where(char.IsDigit).ToArray());
            var seed = int.Parse(soloNumeros.Substring(0, 5));
            Random rand = new Random(seed);
            return rand.Next(minimo, maximo);
        }
    }
}