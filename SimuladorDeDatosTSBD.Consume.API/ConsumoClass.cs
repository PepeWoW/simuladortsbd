﻿using SimuladorDeDatosTSBD.COMMON.Entidades;
using SimuladorDeDatosTSBD.SN.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

namespace SimuladorDeDatosTSBD.Consume.API
{
    public class ConsumoClass
    {
        public NombreCompletoModel GetAlumnosForecast(string gen)
        {
            var url = $"https://simuladordedatostsbdsnapi20191204011322.azurewebsites.net/api/nombrescompletos/{gen}";
            // Synchronous Consumption
            var syncClient = new WebClient();
            NombreCompletoModel weatherData;
            var content = syncClient.DownloadString(url);
            // Create the Json serializer and parse the response
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(NombreCompletoModel));
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
            {
                weatherData = (NombreCompletoModel)serializer.ReadObject(ms);
            }
            return weatherData;
        }
    }
}
